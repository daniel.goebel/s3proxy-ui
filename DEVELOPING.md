# Development Setup

## Install Dependencies
Install dependencies with `npm`.
```shell
npm install
```

## Environment Setup
This has to be done only once. Export the environment variables with the appropriate values and use the `envsubst` command
to populate the template and create the file the `public/env.js`
```shell
export S3PROXY_API_BASE_URL=http://localhost:9999/api/s3proxy-service
export S3URL=<rgw-endpoint>
envsubst < src/assets/env.template.js > public/env.js
```
## Backend Setup
Clone the [repository](https://gitlab.ub.uni-bielefeld.de/daniel.goebel/s3proxy-backend) for the backend and set up the
service for developing.

## Run linter and type check
```shell
npm run lint
npm run type-check
```

## Run Dev Server
Start the dev service with `npm`
```shell
npm run dev
```

## Create axios client
To create the axios client from the OpenAPI definition of the backend, start the backend and execute the following command
```shell
npm run generate-s3proxzßclient
```
