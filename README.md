# S3Proxy-UI

## Description

This is the Frontend to manage your S3 Buckets.

## Environment Variables

The docker container replaces them in the `env.template.js` file and moves that file to the same location as
the `index.html`.
When accessing the website, these variables will be loaded dynamically into the application.

| Variable                | Default | Value    | Description                                      |
|-------------------------|---------|----------|--------------------------------------------------|
| `S3PROXY_API_BASE_URL`  | unset   | HTTP URL | Base URL for the S3Proxy Service API             |
| `S3_URL`                | unset   | HTTP URL | URL of the S3 storage to interact with           |


## Developing

See [DEVELOPING.md](DEVELOPING.md) for a guide how to setup a development instance.

## License

The API is licensed under the [Apache 2.0](https://www.apache.org/licenses/LICENSE-2.0) license. See
the [License](LICENSE) file for more information.
