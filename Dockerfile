# build stage
FROM node:20 as build-stage
WORKDIR /app
COPY package.json ./
COPY package-lock.json ./
RUN npm install --no-fund
COPY . .
RUN npm run build-only

# production stage
FROM nginx:stable-alpine as production-stage
EXPOSE 80
HEALTHCHECK --interval=5s --timeout=2s CMD curl --head -f http://localhost || exit 1
COPY nginx.conf /etc/nginx/conf.d/default.conf
COPY --from=build-stage /app/dist /usr/share/nginx/html
COPY --from=build-stage /app/src/assets/env.template.js /tmp

ARG GIT_COMMIT_HASH
ENV GIT_COMMIT_HASH=${GIT_COMMIT_HASH}

CMD ["/bin/sh",  "-c",  "envsubst < /tmp/env.template.js > /usr/share/nginx/html/env.js && exec nginx -g 'daemon off;'"]
