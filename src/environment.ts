/* eslint-disable @typescript-eslint/ban-ts-comment */
// @ts-ignore
const windowEnv: Record<string, string> = window["env"];
export const environment: env = {
  S3_URL: windowEnv["s3Url"],
  S3PROXY_API_BASE_URL: windowEnv["s3proxyApiUrl"],
};

type env = {
  S3_URL: string;
  S3PROXY_API_BASE_URL: string;
};
