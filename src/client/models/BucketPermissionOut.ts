/* generated using openapi-typescript-codegen -- do not edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { Permission } from './Permission';
/**
 * Schema for the bucket permissions.
 */
export type BucketPermissionOut = {
    /**
     * Start date of permission as UNIX timestamp
     */
    from_timestamp?: (number | null);
    /**
     * End date of permission as UNIX timestamp
     */
    to_timestamp?: (number | null);
    /**
     * Prefix of subfolder
     */
    file_prefix?: (string | null);
    /**
     * Permission
     */
    permission?: (Permission | string);
    /**
     * UID of the grantee
     */
    uid: string;
    /**
     * Name of Bucket
     */
    bucket_name: string;
};

