/* generated using openapi-typescript-codegen -- do not edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
/**
 * Schema for a S3 key associated with a user.
 */
export type S3Key = {
    /**
     * UID of the user of that access key
     */
    uid: string;
    /**
     * ID of the S3 access key
     */
    access_key: string;
    /**
     * Secret of the S3 access key
     */
    secret_key: string;
};

