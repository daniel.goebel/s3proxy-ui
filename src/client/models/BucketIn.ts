/* generated using openapi-typescript-codegen -- do not edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
/**
 * Schema for creating a new bucket.
 */
export type BucketIn = {
    /**
     * Name of the bucket
     */
    name: string;
    /**
     * Description of the bucket
     */
    description: string;
};

