/* generated using openapi-typescript-codegen -- do not edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { Permission } from './Permission';
/**
 * Schema for the parameters of a bucket permission.
 */
export type BucketPermissionParameters = {
    /**
     * Start date of permission as UNIX timestamp
     */
    from_timestamp?: (number | null);
    /**
     * End date of permission as UNIX timestamp
     */
    to_timestamp?: (number | null);
    /**
     * Prefix of subfolder
     */
    file_prefix?: (string | null);
    /**
     * Permission
     */
    permission?: (Permission | string);
};

