/* generated using openapi-typescript-codegen -- do not edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
/**
 * Schema for a error due to a rejected request.
 */
export type ErrorDetail = {
    /**
     * Detail about the occurred error
     */
    detail: string;
};

