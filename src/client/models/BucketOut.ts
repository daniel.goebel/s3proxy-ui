/* generated using openapi-typescript-codegen -- do not edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
/**
 * Schema for answering a request with a bucket.
 */
export type BucketOut = {
    /**
     * Size limit of the bucket in KiB
     */
    size_limit?: (number | null);
    /**
     * Number of objects limit of the bucket
     */
    object_limit?: (number | null);
    /**
     * Name of the bucket
     */
    name: string;
    /**
     * Description of the bucket
     */
    description: string;
    /**
     * Time when the bucket was created as UNIX timestamp
     */
    created_at: number;
    /**
     * UID of the owner
     */
    owner_id: string;
    /**
     * Flag if the bucket is anonymously readable
     */
    public: boolean;
};

