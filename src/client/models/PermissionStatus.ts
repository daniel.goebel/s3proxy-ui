/* generated using openapi-typescript-codegen -- do not edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
/**
 * Status of a bucket permission. Can be either `ACTIVE` or `INACTIVE`. A permission can only get `INACTIVE` if the
 * permission itself has a time limit and the current time is not in the timespan.
 */
export enum PermissionStatus {
    ACTIVE = 'ACTIVE',
    INACTIVE = 'INACTIVE',
}
