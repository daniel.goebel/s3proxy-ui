/* generated using openapi-typescript-codegen -- do not edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
/**
 * Schema for a user.
 */
export type UserOut = {
    /**
     * Full Name of the user
     */
    display_name: string;
    /**
     * ID of the user
     */
    uid: string;
};

