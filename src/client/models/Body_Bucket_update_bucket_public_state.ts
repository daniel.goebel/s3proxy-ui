/* generated using openapi-typescript-codegen -- do not edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
export type Body_Bucket_update_bucket_public_state = {
    /**
     * New State
     */
    public: boolean;
};

