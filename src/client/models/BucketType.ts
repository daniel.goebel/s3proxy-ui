/* generated using openapi-typescript-codegen -- do not edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
/**
 * Enumeration for the type of buckets to fetch from the DB
 *
 * OWN: Only fetch buckets that the user owns
 * PERMISSION: Only fetch foreign buckets that the user has access to
 * ALL: Fetch all buckets that the user has access to
 */
export enum BucketType {
    OWN = 'OWN',
    ALL = 'ALL',
    PERMISSION = 'PERMISSION',
}
