/* generated using openapi-typescript-codegen -- do not edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
/**
 * Enumeration for the possible permission on a bucket.
 */
export enum Permission {
    READ = 'READ',
    WRITE = 'WRITE',
    READWRITE = 'READWRITE',
}
