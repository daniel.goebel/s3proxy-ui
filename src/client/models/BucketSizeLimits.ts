/* generated using openapi-typescript-codegen -- do not edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
export type BucketSizeLimits = {
    /**
     * Size limit of the bucket in KiB
     */
    size_limit?: (number | null);
    /**
     * Number of objects limit of the bucket
     */
    object_limit?: (number | null);
};

