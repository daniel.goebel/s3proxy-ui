/* generated using openapi-typescript-codegen -- do not edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { Body_Bucket_update_bucket_public_state } from '../models/Body_Bucket_update_bucket_public_state';
import type { BucketIn } from '../models/BucketIn';
import type { BucketOut } from '../models/BucketOut';
import type { BucketSizeLimits } from '../models/BucketSizeLimits';
import type { BucketType } from '../models/BucketType';
import type { CancelablePromise } from '../core/CancelablePromise';
import { OpenAPI } from '../core/OpenAPI';
import { request as __request } from '../core/request';
export class BucketService {
    /**
     * List buckets of user
     * List all the buckets in the system or of the desired user where the user has READ permissions for.
     * @param ownerId UID of the user for whom to fetch the buckets for. Permission 'bucket:read_any' required if current user is not the target.
     * @param bucketType Type of the bucket to get. Ignored when `user` parameter not set
     * @returns BucketOut Successful Response
     * @throws ApiError
     */
    public static bucketListBuckets(
        ownerId?: string,
        bucketType?: BucketType,
    ): CancelablePromise<Array<BucketOut>> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/buckets',
            query: {
                'owner_id': ownerId,
                'bucket_type': bucketType,
            },
            errors: {
                400: `Error decoding JWT Token`,
                401: `Not authenticated`,
                403: `Not authorized`,
                404: `Entity not Found`,
                422: `Validation Error`,
            },
        });
    }
    /**
     * Create a bucket for the current user
     * Create a bucket for the current user.
     *
     * The name of the bucket has some constraints.
     * For more information see the
     * [Ceph documentation](https://docs.ceph.com/en/quincy/radosgw/s3/bucketops/#constraints)
     * @param requestBody
     * @returns BucketOut Successful Response
     * @throws ApiError
     */
    public static bucketCreateBucket(
        requestBody: BucketIn,
    ): CancelablePromise<BucketOut> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/buckets',
            body: requestBody,
            mediaType: 'application/json',
            errors: {
                400: `Error decoding JWT Token`,
                401: `Not authenticated`,
                403: `Not authorized`,
                404: `Entity not Found`,
                422: `Validation Error`,
            },
        });
    }
    /**
     * Get a bucket by its name
     * Get a bucket by its name if the current user has READ permissions for the bucket.
     * @param bucketName Name of bucket
     * @returns BucketOut Successful Response
     * @throws ApiError
     */
    public static bucketGetBucket(
        bucketName: string,
    ): CancelablePromise<BucketOut> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/buckets/{bucket_name}',
            path: {
                'bucket_name': bucketName,
            },
            errors: {
                400: `Error decoding JWT Token`,
                401: `Not authenticated`,
                403: `Not authorized`,
                404: `Entity not Found`,
                422: `Validation Error`,
            },
        });
    }
    /**
     * Delete a bucket
     * Delete a bucket by its name. Only the owner of the bucket can delete the bucket.
     * @param bucketName Name of bucket
     * @param forceDelete Delete even non-empty bucket
     * @returns void
     * @throws ApiError
     */
    public static bucketDeleteBucket(
        bucketName: string,
        forceDelete: boolean = false,
    ): CancelablePromise<void> {
        return __request(OpenAPI, {
            method: 'DELETE',
            url: '/buckets/{bucket_name}',
            path: {
                'bucket_name': bucketName,
            },
            query: {
                'force_delete': forceDelete,
            },
            errors: {
                400: `Error decoding JWT Token`,
                401: `Not authenticated`,
                403: `Not authorized`,
                404: `Entity not Found`,
                422: `Validation Error`,
            },
        });
    }
    /**
     * Update public status
     * Update the buckets public state.
     * @param bucketName Name of bucket
     * @param requestBody
     * @returns BucketOut Successful Response
     * @throws ApiError
     */
    public static bucketUpdateBucketPublicState(
        bucketName: string,
        requestBody: Body_Bucket_update_bucket_public_state,
    ): CancelablePromise<BucketOut> {
        return __request(OpenAPI, {
            method: 'PATCH',
            url: '/buckets/{bucket_name}/public',
            path: {
                'bucket_name': bucketName,
            },
            body: requestBody,
            mediaType: 'application/json',
            errors: {
                400: `Error decoding JWT Token`,
                401: `Not authenticated`,
                403: `Not authorized`,
                404: `Entity not Found`,
                422: `Validation Error`,
            },
        });
    }
    /**
     * Update bucket limits
     * Update the buckets size limits.
     * @param bucketName Name of bucket
     * @param requestBody
     * @returns BucketOut Successful Response
     * @throws ApiError
     */
    public static bucketUpdateBucketLimits(
        bucketName: string,
        requestBody: BucketSizeLimits,
    ): CancelablePromise<BucketOut> {
        return __request(OpenAPI, {
            method: 'PATCH',
            url: '/buckets/{bucket_name}/limits',
            path: {
                'bucket_name': bucketName,
            },
            body: requestBody,
            mediaType: 'application/json',
            errors: {
                400: `Error decoding JWT Token`,
                401: `Not authenticated`,
                403: `Not authorized`,
                404: `Entity not Found`,
                422: `Validation Error`,
            },
        });
    }
}
