/* generated using openapi-typescript-codegen -- do not edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { S3Key } from '../models/S3Key';
import type { CancelablePromise } from '../core/CancelablePromise';
import { OpenAPI } from '../core/OpenAPI';
import { request as __request } from '../core/request';
export class S3KeyService {
    /**
     * Get the S3 Access keys from a user
     * Get all the S3 Access keys for a specific user.
     * @param uid UID of a user
     * @returns S3Key Successful Response
     * @throws ApiError
     */
    public static s3KeyGetUserKeys(
        uid: string,
    ): CancelablePromise<Array<S3Key>> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/users/{uid}/keys',
            path: {
                'uid': uid,
            },
            errors: {
                400: `Error decoding JWT Token`,
                401: `Not authenticated`,
                403: `Not authorized`,
                404: `Entity not Found`,
                422: `Validation Error`,
            },
        });
    }
    /**
     * Create a Access key for a user
     * Create a S3 Access key for a specific user.
     * @param uid UID of a user
     * @returns S3Key Successful Response
     * @throws ApiError
     */
    public static s3KeyCreateUserKey(
        uid: string,
    ): CancelablePromise<S3Key> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/users/{uid}/keys',
            path: {
                'uid': uid,
            },
            errors: {
                400: `Error decoding JWT Token`,
                401: `Not authenticated`,
                403: `Not authorized`,
                404: `Entity not Found`,
                422: `Validation Error`,
            },
        });
    }
    /**
     * Get a specific S3 Access key from a user
     * Get a specific S3 Access Key for a specific user.
     * @param accessId ID of the S3 access key
     * @param uid UID of a user
     * @returns S3Key Successful Response
     * @throws ApiError
     */
    public static s3KeyGetUserKey(
        accessId: string,
        uid: string,
    ): CancelablePromise<S3Key> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/users/{uid}/keys/{access_id}',
            path: {
                'access_id': accessId,
                'uid': uid,
            },
            errors: {
                400: `Error decoding JWT Token`,
                401: `Not authenticated`,
                403: `Not authorized`,
                404: `Entity not Found`,
                422: `Validation Error`,
            },
        });
    }
    /**
     * Delete a specific S3 Access key from a user
     * Delete a specific S3 Access key for a specific user.
     *
     * Permission `s3_key:delete` required if the current user is the target, otherwise `s3_key:delete_any` required.
     * @param accessId ID of the S3 access key
     * @param uid UID of a user
     * @returns void
     * @throws ApiError
     */
    public static s3KeyDeleteUserKey(
        accessId: string,
        uid: string,
    ): CancelablePromise<void> {
        return __request(OpenAPI, {
            method: 'DELETE',
            url: '/users/{uid}/keys/{access_id}',
            path: {
                'access_id': accessId,
                'uid': uid,
            },
            errors: {
                400: `Error decoding JWT Token`,
                401: `Not authenticated`,
                403: `Not authorized`,
                404: `Entity not Found`,
                422: `Validation Error`,
            },
        });
    }
}
