/* generated using openapi-typescript-codegen -- do not edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { UserOut } from '../models/UserOut';
import type { CancelablePromise } from '../core/CancelablePromise';
import { OpenAPI } from '../core/OpenAPI';
import { request as __request } from '../core/request';
export class UserService {
    /**
     * Get the logged in user
     * Return the user associated with the used JWT.
     * @returns UserOut Successful Response
     * @throws ApiError
     */
    public static userGetLoggedInUser(): CancelablePromise<UserOut> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/users/me',
            errors: {
                400: `Error decoding JWT Token`,
                401: `Not authenticated`,
                403: `Not authorized`,
                404: `Entity not Found`,
            },
        });
    }
    /**
     * List users and search by their name
     * Return the users that have a specific substring in their name.
     *
     * Permission `user:read_any` required, except when `name_substring` as only query parameter is set,
     * then permission `user:search` required.
     * @param nameSubstring Filter users by a substring in their name.
     * @returns UserOut Successful Response
     * @throws ApiError
     */
    public static userListUsers(
        nameSubstring?: string,
    ): CancelablePromise<Array<UserOut>> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/users',
            query: {
                'name_substring': nameSubstring,
            },
            errors: {
                400: `Error decoding JWT Token`,
                401: `Not authenticated`,
                403: `Not authorized`,
                404: `Entity not Found`,
                422: `Validation Error`,
            },
        });
    }
    /**
     * Get a user by its uid
     * Return the user with the specific uid.
     *
     * Permission `user:read` required if the current user has the same uid as `uid` otherwise `user:read_any` required.
     * @param uid UID of a user
     * @returns UserOut Successful Response
     * @throws ApiError
     */
    public static userGetUser(
        uid: string,
    ): CancelablePromise<UserOut> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/users/{uid}',
            path: {
                'uid': uid,
            },
            errors: {
                400: `Error decoding JWT Token`,
                401: `Not authenticated`,
                403: `Not authorized`,
                404: `Entity not Found`,
                422: `Validation Error`,
            },
        });
    }
}
