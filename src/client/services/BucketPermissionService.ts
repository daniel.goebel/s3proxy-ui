/* generated using openapi-typescript-codegen -- do not edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { BucketPermissionIn } from '../models/BucketPermissionIn';
import type { BucketPermissionOut } from '../models/BucketPermissionOut';
import type { BucketPermissionParameters } from '../models/BucketPermissionParameters';
import type { Permission } from '../models/Permission';
import type { PermissionStatus } from '../models/PermissionStatus';
import type { CancelablePromise } from '../core/CancelablePromise';
import { OpenAPI } from '../core/OpenAPI';
import { request as __request } from '../core/request';
export class BucketPermissionService {
    /**
     * Get all permissions.
     * List all the bucket permissions in the system.
     * @param permissionTypes Type of Bucket Permissions to fetch
     * @param permissionStatus Status of Bucket Permissions to fetch
     * @returns BucketPermissionOut Successful Response
     * @throws ApiError
     */
    public static bucketPermissionListPermissions(
        permissionTypes?: Array<Permission>,
        permissionStatus?: PermissionStatus,
    ): CancelablePromise<Array<BucketPermissionOut>> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/permissions',
            query: {
                'permission_types': permissionTypes,
                'permission_status': permissionStatus,
            },
            errors: {
                400: `Error decoding JWT Token`,
                401: `Not authenticated`,
                403: `Not authorized`,
                404: `Entity not Found`,
                422: `Validation Error`,
            },
        });
    }
    /**
     * Create a permission.
     * Create a permission for a bucket and user.
     * @param requestBody
     * @returns BucketPermissionOut Successful Response
     * @throws ApiError
     */
    public static bucketPermissionCreatePermission(
        requestBody: BucketPermissionIn,
    ): CancelablePromise<BucketPermissionOut> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/permissions',
            body: requestBody,
            mediaType: 'application/json',
            errors: {
                400: `Error decoding JWT Token`,
                401: `Not authenticated`,
                403: `Not authorized`,
                404: `Entity not Found`,
                422: `Validation Error`,
            },
        });
    }
    /**
     * Get all permissions for a user.
     * List all the bucket permissions for the given user.
     * @param uid UID of a user
     * @param permissionTypes Type of Bucket Permissions to fetch
     * @param permissionStatus Status of Bucket Permissions to fetch
     * @returns BucketPermissionOut Successful Response
     * @throws ApiError
     */
    public static bucketPermissionListPermissionsPerUser(
        uid: string,
        permissionTypes?: Array<Permission>,
        permissionStatus?: PermissionStatus,
    ): CancelablePromise<Array<BucketPermissionOut>> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/permissions/user/{uid}',
            path: {
                'uid': uid,
            },
            query: {
                'permission_types': permissionTypes,
                'permission_status': permissionStatus,
            },
            errors: {
                400: `Error decoding JWT Token`,
                401: `Not authenticated`,
                403: `Not authorized`,
                404: `Entity not Found`,
                422: `Validation Error`,
            },
        });
    }
    /**
     * Get all permissions for a bucket.
     * List all the bucket permissions for the given bucket.
     * @param bucketName Name of bucket
     * @param permissionTypes Type of Bucket Permissions to fetch
     * @param permissionStatus Status of Bucket Permissions to fetch
     * @returns BucketPermissionOut Successful Response
     * @throws ApiError
     */
    public static bucketPermissionListPermissionsPerBucket(
        bucketName: string,
        permissionTypes?: Array<Permission>,
        permissionStatus?: PermissionStatus,
    ): CancelablePromise<Array<BucketPermissionOut>> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/permissions/bucket/{bucket_name}',
            path: {
                'bucket_name': bucketName,
            },
            query: {
                'permission_types': permissionTypes,
                'permission_status': permissionStatus,
            },
            errors: {
                400: `Error decoding JWT Token`,
                401: `Not authenticated`,
                403: `Not authorized`,
                404: `Entity not Found`,
                422: `Validation Error`,
            },
        });
    }
    /**
     * Get permission for bucket and user combination.
     * Get the bucket permissions for the specific combination of bucket and user.
     * @param bucketName Name of bucket
     * @param uid UID of a user
     * @returns BucketPermissionOut Successful Response
     * @throws ApiError
     */
    public static bucketPermissionGetPermissionForBucket(
        bucketName: string,
        uid: string,
    ): CancelablePromise<BucketPermissionOut> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/permissions/bucket/{bucket_name}/user/{uid}',
            path: {
                'bucket_name': bucketName,
                'uid': uid,
            },
            errors: {
                400: `Error decoding JWT Token`,
                401: `Not authenticated`,
                403: `Not authorized`,
                404: `Entity not Found`,
                422: `Validation Error`,
            },
        });
    }
    /**
     * Delete a bucket permission
     * Delete the bucket permissions for the specific combination of bucket and user.
     * @param bucketName Name of bucket
     * @param uid UID of a user
     * @returns void
     * @throws ApiError
     */
    public static bucketPermissionDeletePermission(
        bucketName: string,
        uid: string,
    ): CancelablePromise<void> {
        return __request(OpenAPI, {
            method: 'DELETE',
            url: '/permissions/bucket/{bucket_name}/user/{uid}',
            path: {
                'bucket_name': bucketName,
                'uid': uid,
            },
            errors: {
                400: `Error decoding JWT Token`,
                401: `Not authenticated`,
                403: `Not authorized`,
                404: `Entity not Found`,
                422: `Validation Error`,
            },
        });
    }
    /**
     * Update a bucket permission
     * Update a permission for a bucket and user.
     * @param bucketName Name of bucket
     * @param uid UID of a user
     * @param requestBody
     * @returns BucketPermissionOut Successful Response
     * @throws ApiError
     */
    public static bucketPermissionUpdatePermission(
        bucketName: string,
        uid: string,
        requestBody: BucketPermissionParameters,
    ): CancelablePromise<BucketPermissionOut> {
        return __request(OpenAPI, {
            method: 'PUT',
            url: '/permissions/bucket/{bucket_name}/user/{uid}',
            path: {
                'bucket_name': bucketName,
                'uid': uid,
            },
            body: requestBody,
            mediaType: 'application/json',
            errors: {
                400: `Error decoding JWT Token`,
                401: `Not authenticated`,
                403: `Not authorized`,
                404: `Entity not Found`,
                422: `Validation Error`,
            },
        });
    }
}
