/* generated using openapi-typescript-codegen -- do not edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { OIDCProvider } from '../models/OIDCProvider';
import type { CancelablePromise } from '../core/CancelablePromise';
import { OpenAPI } from '../core/OpenAPI';
import { request as __request } from '../core/request';
export class AuthService {
    /**
     * Redirect to LifeScience OIDC Login
     * Redirect route to OIDC provider to kickstart the login process.
     * @param provider The OIDC provider to use for login
     * @param returnPath Will be appended to redirect response in the callback route as URL query parameter `return_path`
     * @returns void
     * @throws ApiError
     */
    public static authLogin(
        provider?: OIDCProvider,
        returnPath?: string,
    ): CancelablePromise<void> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/auth/login',
            query: {
                'provider': provider,
                'return_path': returnPath,
            },
            errors: {
                302: `Successful Response`,
                422: `Validation Error`,
            },
        });
    }
    /**
     * LifeScience Login Callback
     * Callback for the Life Science Identity Provider.
     *
     * Visit the route login route to start the login process.
     *
     * If the user is already known to the system, then a JWT token will be created and sent via the 'set-cookie' header.
     * The key for this Cookie is 'bearer'.
     *
     * If the user is new, he will be created and then a JWT token is issued.
     *
     * This JWT has to be sent to all authorized endpoints via the HTTPBearer scheme.
     * @param provider
     * @returns void
     * @throws ApiError
     */
    public static authLoginCallback(
        provider: OIDCProvider,
    ): CancelablePromise<void> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/auth/callback/{provider}',
            path: {
                'provider': provider,
            },
            errors: {
                302: `Successful Response`,
                422: `Validation Error`,
            },
        });
    }
}
