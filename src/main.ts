import { createApp } from "vue";
import { createPinia } from "pinia";

import App from "./App.vue";
import router from "./router";

import dayjs from "dayjs";
import relativeTime from "dayjs/plugin/relativeTime";
import isSameOrBefore from "dayjs/plugin/isSameOrBefore";
import "dayjs/locale/en-gb";
import utc from "dayjs/plugin/utc";
import timezone from "dayjs/plugin/timezone";

dayjs.extend(relativeTime); // use plugin
dayjs.extend(isSameOrBefore);
dayjs.extend(utc);
dayjs.extend(timezone);
dayjs.tz.setDefault(dayjs.tz.guess());

import "bootstrap/dist/css/bootstrap.css";
import "@fortawesome/fontawesome-free/css/fontawesome.css";
import "@fortawesome/fontawesome-free/css/solid.css";
import "@fortawesome/fontawesome-free/css/brands.css";

import "./assets/main.css";

import { globalCookiesConfig } from "vue3-cookies";

globalCookiesConfig({
  expireTimes: "8d",
  domain: window.location.hostname,
});

const app = createApp(App);

app.use(createPinia());
app.use(router);

app.mount("#app");

// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { Modal, Dropdown, Tooltip, Toast } from "bootstrap";
