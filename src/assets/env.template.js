(function (window) {
  window["env"] = window["env"] || {};

  // Environment variables
  window["env"]["s3Url"] = "${S3_URL}";
  window["env"]["s3proxyApiUrl"] = "${S3PROXY_API_BASE_URL}";
})(this);
