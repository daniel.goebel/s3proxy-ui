import { defineStore } from "pinia";

export const useNameStore = defineStore({
  id: "names",
  state: () =>
    ({
      nameMapping: {},
    }) as {
      nameMapping: Record<string, string>;
    },
  getters: {
    getName(): (objectID?: string | null) => string | undefined {
      return (objectID) => {
        if (objectID) {
          return this.nameMapping[objectID] ?? localStorage.getItem(objectID);
        }
        return undefined;
      };
    },
  },
  actions: {
    addNameToMapping(objectId: string, objectName: string) {
      this.nameMapping[objectId] = objectName;
      localStorage.setItem(objectId, objectName);
    },
    deleteNameFromMapping(objectId: string) {
      delete this.nameMapping[objectId];
      localStorage.removeItem(objectId);
    },
    loadNameMapping() {
      if (Object.keys(this.nameMapping).length > 0) {
        return;
      }
      for (let i = 0; i < localStorage.length; i++) {
        const key = localStorage.key(i);
        if (key != null) {
          const value = localStorage.getItem(key);
          if (value != null) {
            this.nameMapping[key] = value;
          }
        }
      }
    },
  },
});
