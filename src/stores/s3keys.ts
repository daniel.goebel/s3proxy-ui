import { defineStore } from "pinia";
import { useAuthStore } from "@/stores/users";
import type { S3Key } from "@/client";
import { S3KeyService } from "@/client";
import { useS3ObjectStore } from "@/stores/s3objects";

export const useS3KeyStore = defineStore({
  id: "s3keys",
  state: () =>
    ({
      keyMapping: {},
    }) as {
      keyMapping: Record<string, S3Key>;
    },
  getters: {
    keys(): S3Key[] {
      const tempList = Object.values(this.keyMapping);
      tempList.sort((keyA, keyB) =>
        keyA.access_key > keyB.access_key ? 1 : -1,
      );
      return tempList;
    },
  },
  actions: {
    fetchS3Keys(onFinally?: () => void): Promise<S3Key[]> {
      if (this.keys.length > 0) {
        onFinally?.();
      }
      return S3KeyService.s3KeyGetUserKeys(useAuthStore().currentUID)
        .then((keys) => {
          const s3ObjectRepository = useS3ObjectStore();
          s3ObjectRepository.updateS3Client(keys[0]);
          const newMapping: Record<string, S3Key> = {};
          for (const key of keys) {
            newMapping[key.access_key] = key;
          }
          this.keyMapping = newMapping;
          return keys;
        })
        .finally(onFinally);
    },
    deleteS3Key(access_id: string): Promise<void> {
      const userRepository = useAuthStore();
      return S3KeyService.s3KeyDeleteUserKey(
        access_id,
        userRepository.currentUID,
      ).then(() => {
        const s3ObjectRepository = useS3ObjectStore();
        delete this.keyMapping[access_id];
        s3ObjectRepository.updateS3Client(this.keys[0]);
      });
    },
    createS3Key(): Promise<S3Key> {
      const userRepository = useAuthStore();
      return S3KeyService.s3KeyCreateUserKey(userRepository.currentUID).then(
        (key) => {
          this.keyMapping[key.access_key] = key;
          return key;
        },
      );
    },
  },
});
