import { defineStore } from "pinia";
import { BucketPermissionService, BucketService, BucketType } from "@/client";
import type {
  BucketOut,
  BucketIn,
  BucketPermissionOut,
  BucketPermissionIn,
  BucketPermissionParameters,
} from "@/client";
import { useAuthStore } from "@/stores/users";

export const useBucketStore = defineStore({
  id: "buckets",
  state: () =>
    ({
      bucketMapping: {},
      ownPermissions: {},
      bucketPermissionsMapping: {},
    }) as {
      bucketMapping: Record<string, BucketOut>;
      ownPermissions: Record<string, BucketPermissionOut>;
      bucketPermissionsMapping: Record<string, BucketPermissionOut[]>;
    },
  getters: {
    buckets(): BucketOut[] {
      const tempList = Object.values(this.bucketMapping);
      tempList.sort((bucketA, bucketB) => {
        return bucketA.name > bucketB.name ? 1 : -1;
      });
      return tempList;
    },
    ownBuckets(): BucketOut[] {
      const authStore = useAuthStore();
      return this.buckets.filter(
        (bucket) => bucket.owner_id === authStore.currentUID,
      );
    },
    getBucketPermissions(): (bucketName: string) => BucketPermissionOut[] {
      return (bucketName) => this.bucketPermissionsMapping[bucketName] ?? [];
    },
    permissionFeatureAllowed(): (bucketName: string) => boolean {
      const authStore = useAuthStore();
      return (bucketName) => {
        // If a permission for the bucket exist, then false
        if (this.ownPermissions[bucketName] != undefined) {
          return false;
        }
        // If the bucket doesn't exist, then false
        return this.bucketMapping[bucketName]?.owner_id == authStore.currentUID;
      };
    },
    writableBucket(): (bucketName: string) => boolean {
      const authStore = useAuthStore();
      return (bucketName) => {
        // If this is a foreign bucket, check that the user has write permission
        if (this.ownPermissions[bucketName] != undefined) {
          return this.ownPermissions[bucketName].permission !== "READ";
        }
        return this.bucketMapping[bucketName]?.owner_id == authStore.currentUID;
      };
    },
    readableBucket(): (bucketName: string) => boolean {
      const authStore = useAuthStore();
      return (bucketName) => {
        // If this is a foreign bucket, check that the user has read permission
        if (this.ownPermissions[bucketName] != undefined) {
          return this.ownPermissions[bucketName].permission !== "WRITE";
        }
        return this.bucketMapping[bucketName]?.owner_id == authStore.currentUID;
      };
    },
  },
  actions: {
    fetchBuckets(
      ownerId?: string,
      bucketType?: BucketType,
    ): Promise<BucketOut[]> {
      return BucketService.bucketListBuckets(ownerId, bucketType).then(
        (buckets) => {
          const userRepository = useAuthStore();
          userRepository.fetchUsernames(
            buckets
              .map((bucket) => bucket.owner_id)
              .filter((owner) => owner != userRepository.currentUID),
          );
          return buckets;
        },
      );
    },
    fetchOwnPermissions(
      onFinally?: () => void,
    ): Promise<BucketPermissionOut[]> {
      const authStore = useAuthStore();
      if (Object.keys(this.ownPermissions).length > 0) {
        onFinally?.();
      }
      return BucketPermissionService.bucketPermissionListPermissionsPerUser(
        authStore.currentUID,
      )
        .then((permissions) => {
          const new_permissions: Record<string, BucketPermissionOut> = {};
          for (const perm of permissions) {
            new_permissions[perm.bucket_name] = perm;
          }
          this.ownPermissions = new_permissions;
          return permissions;
        })
        .finally(onFinally);
    },
    deleteOwnPermission(bucketName: string) {
      delete this.ownPermissions[bucketName];
      delete this.bucketMapping[bucketName];
    },
    fetchOwnBuckets(onFinally?: () => void): Promise<BucketOut[]> {
      if (this.ownBuckets.length > 0) {
        onFinally?.();
      }
      const userStore = useAuthStore();
      return this.fetchBuckets(userStore.currentUID)
        .then((buckets) => {
          for (const bucket of buckets) {
            this.bucketMapping[bucket.name] = bucket;
          }
          return buckets;
        })
        .finally(onFinally);
    },
    fetchBucket(
      bucketName: string,
      onFinally?: () => void,
    ): Promise<BucketOut> {
      if (this.bucketMapping[bucketName] != undefined) {
        onFinally?.();
      }
      return BucketService.bucketGetBucket(bucketName)
        .then((bucket) => {
          this.bucketMapping[bucket.name] = bucket;
          return bucket;
        })
        .finally(onFinally);
    },
    deleteBucket(bucketName: string): Promise<void> {
      return BucketService.bucketDeleteBucket(bucketName, true).then(() => {
        delete this.bucketMapping[bucketName];
      });
    },
    createBucket(bucket: BucketIn): Promise<BucketOut> {
      return BucketService.bucketCreateBucket(bucket).then((createdBucket) => {
        this.bucketMapping[createdBucket.name] = createdBucket;
        return createdBucket;
      });
    },
    fetchBucketPermissions(
      bucketName: string,
      onFinally?: () => void,
    ): Promise<BucketPermissionOut[]> {
      if (this.bucketPermissionsMapping[bucketName] != undefined) {
        onFinally?.();
      }
      return BucketPermissionService.bucketPermissionListPermissionsPerBucket(
        bucketName,
      )
        .then((permissions) => {
          this.bucketPermissionsMapping[bucketName] = permissions;
          return permissions;
        })
        .finally(onFinally);
    },
    deleteBucketPermission(bucketName: string, uid: string): Promise<void> {
      return BucketPermissionService.bucketPermissionDeletePermission(
        bucketName,
        uid,
      ).then(() => {
        const userRepository = useAuthStore();
        if (uid == userRepository.currentUID) {
          this.deleteOwnPermission(bucketName);
        }
        if (this.bucketPermissionsMapping[bucketName] == undefined) {
          this.fetchBucketPermissions(bucketName);
        } else {
          this.bucketPermissionsMapping[bucketName] =
            this.bucketPermissionsMapping[bucketName].filter(
              (permission) => permission.uid != uid,
            );
        }
      });
    },
    createBucketPermission(
      permissionIn: BucketPermissionIn,
    ): Promise<BucketPermissionOut> {
      return BucketPermissionService.bucketPermissionCreatePermission(
        permissionIn,
      ).then((permission) => {
        if (
          this.bucketPermissionsMapping[permission.bucket_name] == undefined
        ) {
          this.fetchBucketPermissions(permission.bucket_name);
        } else {
          this.bucketPermissionsMapping[permission.bucket_name].push(
            permission,
          );
        }
        return permission;
      });
    },
    updateBucketPermission(
      bucketName: string,
      uid: string,
      permissionParams: BucketPermissionParameters,
    ): Promise<BucketPermissionOut> {
      return BucketPermissionService.bucketPermissionUpdatePermission(
        bucketName,
        uid,
        permissionParams,
      ).then((permissionOut) => {
        if (this.bucketPermissionsMapping[bucketName] == undefined) {
          this.fetchBucketPermissions(bucketName);
        } else {
          const index = this.bucketPermissionsMapping[bucketName].findIndex(
            (permission) => (permission.uid = uid),
          );
          if (index > -1) {
            this.bucketPermissionsMapping[bucketName][index] = permissionOut;
          } else {
            this.bucketPermissionsMapping[bucketName].push(permissionOut);
          }
        }
        return permissionOut;
      });
    },
    async updatePublicState(
      bucketName: string,
      public_: boolean,
    ): Promise<BucketOut> {
      const bucket = await BucketService.bucketUpdateBucketPublicState(
        bucketName,
        {
          public: public_,
        },
      );
      this.bucketMapping[bucketName] = bucket;
      return bucket;
    },
    updateBucketLimits(
      bucketName: string,
      size_limit?: number | null,
      object_limit?: number | null,
    ): Promise<BucketOut> {
      return BucketService.bucketUpdateBucketLimits(bucketName, {
        size_limit: size_limit,
        object_limit: object_limit,
      });
    },
  },
});
