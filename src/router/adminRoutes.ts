import type { RouteRecordRaw } from "vue-router";

export const adminRoutes: RouteRecordRaw[] = [
  {
    path: "admin/users",
    name: "admin-users",
    component: () => import("../views/admin/AdminUsersView.vue"),
    meta: {
      title: "Manage Users",
    },
  },
  {
    path: "admin/buckets",
    name: "admin-buckets",
    component: () => import("../views/admin/AdminBucketsView.vue"),
    meta: {
      title: "Manage Buckets",
    },
  },
];
