import { createRouter, createWebHistory } from "vue-router";
import DashboardView from "../views/DashboardView.vue";
import LoginView from "../views/LoginView.vue";
import { s3Routes } from "@/router/s3Routes";
import { adminRoutes } from "@/router/adminRoutes";
import ImprintView from "@/views/ImprintView.vue";
import PrivacyPolicyView from "@/views/PrivacyPolicyView.vue";
import TermsOfUsageView from "@/views/TermsOfUsageView.vue";

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/dashboard",
      name: "dashboard",
      component: DashboardView,
      children: [...s3Routes, ...adminRoutes],
    },
    {
      path: "/login",
      name: "login",
      component: LoginView,
      meta: {
        title: "Login",
      },
      props: (route) => ({
        returnPath: route.query.return_path ?? undefined,
        loginError: route.query.login_error ?? undefined,
      }),
    },
    {
      path: "/",
      redirect: {
        name: "buckets",
      },
    },
    {
      path: "/privacy",
      name: "privacy",
      meta: {
        title: "Privacy Policy",
      },
      component: PrivacyPolicyView,
    },
    {
      path: "/terms",
      name: "terms",
      meta: {
        title: "Terms of Usage",
      },
      component: TermsOfUsageView,
    },
    {
      path: "/imprint",
      name: "imprint",
      meta: {
        title: "Imprint",
      },
      component: ImprintView,
    },
    {
      path: "/:pathMatch(.*)",
      meta: {
        title: "Error",
      },
      component: () => import("../views/NotFoundView.vue"),
    },
  ],
});

router.afterEach((to) => {
  if (to.meta?.title) {
    document.title = to.meta.title + " - S3Proxy";
  }
});

export default router;
